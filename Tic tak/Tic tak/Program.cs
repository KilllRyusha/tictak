﻿using System;
namespace ConsoleApp1DG
{
    class Program
    {
        static string[,] gameField = new string[3, 3];
        static void Main(string[] args)
        {
            InitField();

            while (true)
            {
                DrawField();
                PlayerMove();
                AIMove();
                EventCheck("x");
                EventCheck("0");
                //ShutDownEverything();
            }
        }
        static void AIMove()
        {
            Random rnd = new Random();
            bool isEmpty = false;
            int x = rnd.Next(0, 3);
            int y = rnd.Next(0, 3);

            while (isEmpty==false
                )
            {
                if (!gameField[x, y].Equals("*"))
                {
                    x = rnd.Next(0, 3);
                    y = rnd.Next(0, 3);
                }
                else
                {
                    gameField[x, y] = "0";
                    Console.WriteLine(gameField[x, y]);
                    isEmpty = true;
                }
            }
        }
        static void Final()
        {
            Console.WriteLine("Hope you'll come later " +
                " " +
                "I see you!!!");

        }
        static void EventCheck(string val)
        {
            if (gameField[0, 0].Equals(val) && gameField[0, 1].Equals(val) && gameField[0, 2].Equals(val) &&
            gameField[1, 0].Equals(val) && gameField[1, 1].Equals(val) && gameField[1, 2].Equals(val) &&
            gameField[2, 0].Equals(val) && gameField[2, 1].Equals(val) && gameField[2, 2].Equals(val) &&
            gameField[0, 0].Equals(val) && gameField[1, 0].Equals(val) && gameField[2, 0].Equals(val) &&
            gameField[0, 1].Equals(val) && gameField[1, 1].Equals(val) && gameField[2, 1].Equals(val) &&
            gameField[0, 2].Equals(val) && gameField[1, 2].Equals(val) && gameField[2, 2].Equals(val) &&
            gameField[0, 0].Equals(val) && gameField[1, 1].Equals(val) && gameField[2, 2].Equals(val) &&
            gameField[0, 2].Equals(val) && gameField[1, 1].Equals(val) && gameField[2, 0].Equals(val))
            {
                Console.WriteLine("Do You want some more?(y or n(Press n two times))");
                if (Convert.ToString(Console.ReadLine()) == "y")
                {
                    InitField();
                }
                else if (Convert.ToString(Console.ReadLine()) == "n")
                {

                    Final();
                    Environment.Exit(0);

                }
            }
                if (gameField[0, 0].Equals(val) && gameField[0, 1].Equals(val) && gameField[0, 2].Equals(val) ||
            gameField[1, 0].Equals(val) && gameField[1, 1].Equals(val) && gameField[1, 2].Equals(val) ||
            gameField[2, 0].Equals(val) && gameField[2, 1].Equals(val) && gameField[2, 2].Equals(val) ||
            gameField[0, 0].Equals(val) && gameField[1, 0].Equals(val) && gameField[2, 0].Equals(val) ||
            gameField[0, 1].Equals(val) && gameField[1, 1].Equals(val) && gameField[2, 1].Equals(val) ||
            gameField[0, 2].Equals(val) && gameField[1, 2].Equals(val) && gameField[2, 2].Equals(val) ||
            gameField[0, 0].Equals(val) && gameField[1, 1].Equals(val) && gameField[2, 2].Equals(val) ||
            gameField[0, 2].Equals(val) && gameField[1, 1].Equals(val) && gameField[2, 0].Equals(val))
            {
                Console.WriteLine("THE WINNER IS " + val + " - MAN");

                Console.WriteLine("Do You want some more?(y or n(Press n two times))");
                if (Console.ReadLine().Equals("y"))
                {
                    InitField();
                }
               else if (Console.ReadLine().Equals("n"))
                {
                    
                    Final();
                    Environment.Exit(0);

                }
            }
        }
        static void ShutDownEverything()
        {
            int x = 0;
            int y = 0;
            for (int i = 0; i < gameField.GetLength(0); i++)
            {
                for (int j = 0; j < gameField.GetLength(1); j++)
                {
                    if (gameField[x, y].Equals("*"))
                    {
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
        //static void SaveGame()
        //{
        //    string filePath = @"C:\Users\Dell\Desktop\javaApps\GameData.txt";
        //    string text = "Super text 3000";

        //   // File.WriteAllText(filePath, text);

        //    Console.WriteLine("Saved...");
        //}
        static void InitField()
        {
            for (int i = 0; i < gameField.GetLength(0); i++)
            {
                for (int j = 0; j < gameField.GetLength(1); j++)
                {
                    gameField[i, j] = "*";
                }
            }
        }
        static void DrawField()
        {
            for (int i = 0; i < gameField.GetLength(0); i++)
            {
                Console.WriteLine();
                for (int j = 0; j < gameField.GetLength(1); j++)
                {
                    Console.Write(gameField[i, j] + " ");
                }
            }
        }
        static void PlayerMove()
        {
            int x = 0;
            int y = 0;
            bool isEmpty = false;

            Console.WriteLine("Ход игрока.");
            Console.WriteLine("Введите X: ");
            x = PlayerChoice();
            Console.WriteLine("Введите Y: ");
            y = PlayerChoice();

            while (!isEmpty)
            {
                if (!gameField[x, y].Equals("*"))
                {
                    x = PlayerChoice();
                    y = PlayerChoice();
                }
                else
                {
                    gameField[x, y] = "x";
                    Console.WriteLine(gameField[x, y]);
                    isEmpty = true;
                }
            }
        }
        static int PlayerChoice()
        {
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}